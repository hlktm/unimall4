import {
	base_url
} from './config.js'

class HttpRequest {
	request(url, method, params) {
		uni.showLoading({
			title: '加载中....'
		})
		// console.log(url.indexOf("https"),"http");
		// console.log(uni.getStorageSync("token"),"00token");
		return new Promise((resolve, reject) => {
			uni.request({
				url: url.indexOf("https") == -1 ? base_url + url : url, //接口地址。
				data: params,
				method,
				header: {
					authorization: uni.getStorageSync("token") ? "bearer" + uni.getStorageSync(
						"token") : ""
				},
				success: (res) => {
					// console.log(res.data,'res.data');
					if (res.statusCode == 200) {
						resolve(res.data)
						uni.hideLoading()
					}
				},
				fail: (error) => {
					reject(error)
					uni.hideLoading()
					uni.showToast({
						title: '请求失败',
						duration: 2000
					})
				},
				complete: () => {
					uni.hideLoading()
				}
			})
		})
	}
	get(url, params = {}) {
		return this.request(url, 'GET', params)
	}
	post(url, params) {
		return this.request(url, 'POST', params)
	}
	put(url, params) {
		return this.request(url, 'PUT', params)
	}
	delete(url, params) {
		return this.request(url, 'DELETE', params)
	}
	delete(url,params){
	  return this.request(url,'DELETE',params)
	}
}

export default new HttpRequest()
