import httprequest from '@/utils/httprequset.js'
// 商品个数接口
// export const IndexImage = ()=> httprequest.get('indexImgs')
export const getCart = (data) =>httprequest.post('p/shopCart/info',data)


// p/shopCart/changeItem
export const gettotalPay = (payload) => httprequest.post('p/shopCart/totalPay',payload)

//商品数量
export const getProdCount = () => httprequest.get('p/shopCart/prodCount')
// 加入购物车
export const getChageItem = (payload) =>httprequest.post('p/shopCart/changeItem',payload)
//  删除
export const getdeleteItem = (payload) => httprequest.delete('/p/shopCart/deleteItem',payload)
//   let arr=JSON.parse(JSON.stringify(payload)) 
// /p/shopCart/deleteItem

