import httprequest from '@/utils/httprequset.js'

// 分类数据

export const  categoryInfo= ({url})=> httprequest.get(url);

export const categoryId = (params)=> httprequest.get('prod/pageProd',params);

export const  buyImmediately= ({url})=> httprequest.post(url);

// export const buyImmediately = ()=> httprequest.post('p/order/confirm');
