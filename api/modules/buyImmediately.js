import httprequest from '@/utils/httprequset.js'

// 获取商品数据
export const buyImmediately = (params)=> httprequest.post('p/order/confirm',params);

// 提交
export const submit = (params) => httprequest.post("p/order/submit",params);

// 支付
export const orderpay = (params) => httprequest.post("p/order/pay",params);
