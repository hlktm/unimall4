import httprequest from '@/utils/httprequset.js'
//轮播图
export const IndexImage = ()=> httprequest.get('indexImgs')
//标签列表
export const TagList = ()=> httprequest.get('shop/notice/topNoticeList')
//
export const prodListByTagId = ()=> httprequest.get('prod/tag/prodTagList')
//每日上新  
export const prodListByEvery = (params)=> httprequest.get('prod/prodListByTagId',params)
//新品推荐
export const newProdList = (params)=> httprequest.get('/prod/lastedProdPage',params)
//搜索
export const searchList = (params)=> httprequest.get('/search/searchProdPage',params)
//消息详情 11
export const messageDetailList = (params)=> httprequest.get(`/shop/notice/info/${params}`)



//收藏页面 请求
export const collectPro = (params)=> httprequest.get('p/user/collection/prods',params)