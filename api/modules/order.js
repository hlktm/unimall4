import httpRequest from "@/utils/httprequset.js"
// 获取商品列表信息
export const getOrder = (data)=> httpRequest.get('p/myOrder/myOrder',data)
//  取消订单
export const cancelOrder = (data)=> httpRequest.put('p/myOrder/cancel/'+data)
//   删除订单
export const delOrder = (params)=> httpRequest.delete('p/myOrder/'+params)
// // 获取城市信息
// export const listByLeft = (params)=> httpRequest.get('p/area/listByPid',params)
// // 绑定手机号确认按钮
// export const sureBtn = (params)=> httpRequest.put('user/registerOrBindUser',params)
