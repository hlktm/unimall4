import httpRequest from "@/utils/httprequset.js"
// console.log(httpRequest);
// 登录
export const toLogoin = (data) => httpRequest.post('login?grant_type=mini_app', data)
// 获取验证码
export const getCode = (data) => httpRequest.post('p/sms/send', data)
// 获取地址信息
export const getAddress = () => httpRequest.get('p/address/list')
// 获取城市信息
export const listByLeft = (params) => httpRequest.get('p/area/listByPid', params)
// 绑定手机号确认按钮
export const sureBtn = (params) => httpRequest.put('user/registerOrBindUser', params)
// 新增地址
export const listByPid = (params) => httpRequest.get('p/area/listByPid', params)
// 保存新增地址
export const addADDr = (params) => httpRequest.post('p/address/addAddr', params)
// 保存修改地址
export const redactADDr = (params) => httpRequest.put('p/address/updateAddr', params)
// 删除地址
export const DELADDr = (params) => httpRequest.delete('p/address/deleteAddr/' + params.addrId)
//编辑地址
export const editADDr = (params) => httpRequest.get('p/address/addrInfo/' + params.addrId)
