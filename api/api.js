// 入口文件
const httpRequest = require("../utils/httprequset") 
// console.log(httpRequest,"httpRequest");
const context = require.context('./modules', false, /\.js$/);
// console.log(context.keys(),"keys");
const dataObj = context.keys().reduce((prev, cur) => {
  // console.dir(context(cur));
  return { ...prev, ...context(cur).default};
}, {});

// console.log(dataObj,"dataObj");
const api = Object.keys(dataObj).reduce((prev, cur) => {
  prev[cur] = (method,data) => {
	  // console.log(dataObj[cur].method,"cur");
	  // console.log(httpRequest[dataObj[cur].method],"cur.method");
    return httpRequest({
      ...dataObj[cur],
      [dataObj[cur].method == 'get' ? 'params' : 'data']: data,
    });
  };
  return prev;
}, {});
// console.log(api, 'api555');

export default api;
