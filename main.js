
// #ifndef VUE3


import Vue from 'vue'
import App from './App'
import Plugins from './plugins/index.js'
import Store from './store/index.js'

Vue.config.productionTip = false

App.mpType = 'app'
Vue.use(Plugins,{
	title:"1909B"
})
const app = new Vue({
	store:Store,
    ...App
})
app.$mount()
// #endif

// // #ifdef VUE3
// import { createSSRApp } from 'vue'
// import App from './App.vue'
// export function createApp() {
//   const app = createSSRApp(App)
//   return {
//     app
//   }
// }
// // #endif