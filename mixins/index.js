import {mapState} from 'vuex'
export default {
	data(){
		return {
			total:0
		}
	},
	computed:{
		...mapState('car',['total'])
	},
	onShow(){
		// console.log(12312,this.total)
		this.setTabBarBadge()
	},
	methods:{
		setTabBarBadge(){
			uni.setTabBarBadge({
			  index: 2,
			  text: this.total + ''
			})
		}
	}
}