// let {
// 	Engine,
// 	Scene,
// 	RotatePerspectiveCamera,
// 	Skybox,
// 	Geometry
// } = require("./g3d.min.js") 
 
// let lx = null,
// 	ly = null;
// /**
//  *  
//  * 由于 threejs 太大了...
//  * 先天不足: 由于 wxs不能操作, 微信小程序默认采用前后端分离,消息模式,所以性能和效率上都有损耗,可能不会太流畅
//  * 注意: 微信小程序不支持2048图片  见 https://github.com/deepkolos/three-platformize
//  * 
// 	注意 skybox 从立方体内部看纹理,是反的, 目前是用cdn把图片转了180度解决.  后面如果遇到不支持cdn的,要考虑用canvas进行镜像转换
// 	貌似可以直接用webgl 把贴图直接镜像..... 没成功
  
// 	@mail 35802713@qq.com 
//  */
// class Image360 {
 
// 	/**
// 	 * @param {Object} selector
// 	 * @param {Object} canvas
// 	 * @param {Object} images
// 	 * @param {Object} opt
// 	 */
// 	constructor(canvas, images, opt) {
// 		this.canvas = canvas;
// 		// this.gl = canvas.getContext('webgl');
// 		this.images = images;
// 		this.opt = opt || {
// 			//贴图全部加载完成的回调事件
// 			textureComplete: (image360) => {}
// 		}
// 		this.init();
// 	}
 
// 	/** 
// 	 * 初始化 
// 	 */
// 	init() {
// 		let self = this;
// 		this.engine = new Engine(self.canvas);
 
// 		this.scene = new Scene(this.engine);
 
// 		this.camera = new RotatePerspectiveCamera(this.scene);
// 		 //横向角度
// 		this.camera.alpha = 0;
// 		//纵向角度
// 		this.camera.beta = 0;
// 		this.camera.radius =10;// 不用改,大了看起来像镜中地球
// 		this.camera.near = 0.001;
// 		this.camera.far = 2000;
// // console.log(this.camera,this.scene)
// 		let cnt = 0; 
// 		// fbudlr 
// 		let imageListPOsi = [ "back","front", "top","bottom", "left", "right"];
 
// 		let pos = {}
// 		for (let i = 0; i < this.images.length; i++) {
// 			let image = this.canvas.createImage(); //和h5区别 小程序这里没有 new Image
// 			pos[imageListPOsi[i]] = image;
// 			image.onload = () => {
// 				cnt++;
// 				if (cnt >= 6) { //pano形式最多6张,也就是正方体的6个面 
// 					let skybox = new Skybox(self.scene, pos,100,true);
// 					console.log(skybox)
// 					function render() {
// 						self.scene.render();
// 						self.requestAnimationFrame(render);
// 					}
 
// 					render();
 
// 					//贴图全部加载完成后的回调,外部调用
// 					self.opt.textureComplete && self.opt.textureComplete(self);
// 				}
// 			}
// 			image.src = this.images[i];
// 		}
 
 
// 	}
 
 
 
 
// 	touchmove(e) {
// 		let self = this;
// 		//console.log(e)
// 		let x = e.touches[0].clientX;
// 		let y = e.touches[0].clientY;
 
// 		self.camera.alpha += (x - lx) / 5;
// 		this.camera.beta = self.clamp(-90, 90, this.camera.beta - (y - ly) / 5); 
// 		lx = x;
// 		ly = y;
// 	}
 
// 	touchstart(e) {
// 		let self = this;
// 		//console.log(e)
// 		let x = e.touches[0].clientX;
// 		let y = e.touches[0].clientY;
// 		lx = x;
// 		ly = y;
// 	}
 
// 	touchend(e) {
 
// 	}
 
// 	/**
// 	 * 重绘界面 就认为是动画呈现吧
// 	 * 见 https://developers.weixin.qq.com/miniprogram/dev/api/canvas/Canvas.requestAnimationFrame.html
// 	 */
// 	requestAnimationFrame(cb) {
// 		this.canvas.requestAnimationFrame(cb);
// 	}
 
// 	clamp(min, max, v) {
// 		return v < min ? min : v > max ? max : v;
// 	}
// }
 
 
// export default Image360