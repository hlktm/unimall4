import {
	getOrder
} from "@/api/modules/order.js";
const order = {
	namespaced: true, //开启命名空间
	state: () => ({
		current: 1, //当前页
		size: 10, //每页条数
		active: 0, //
		isActive: 0, //  页面高亮
		status: 0, // 第几个页面
		records: [], //请求回来的数据
	}),
	mutations: {
		setOrder(state, payload) {
			// console.log(payload,"payload");
			state.current = payload.current;
			state.size = payload.size;
			state.status = payload.status;
			state.records = payload.records;
		}
	},
	actions: {
		async orderGet({
			commit
		}, paylaod) {
			// console.log("orderGet");
			let res = await getOrder({
				current: paylaod.current,
				size: paylaod.size,
				status: paylaod.status
			})
			// console.log(res,"orderres");
			commit("setOrder", res)
		}

	}
}

export default order;
