import {
	toLogoin
} from "../../api/modules/login.js";
const total = {
	namespaced: true, //开启命名空间
	state: () => ({
		userInfo: uni.getStorageSync("userInfo") || {},
		token: uni.getStorageSync("token") || "",
	}),
	mutations: {
		// 登录
		getUser(state, payload) {
			state.userInfo = payload.userInfo;
			state.token = payload.token;
			uni.setStorageSync("token", payload.token)
			uni.setStorageSync("userInfo", payload.userInfo)
		},
		// 退出登录
		removeStorage(state) {
			state.userInfo = {};
			state.token = "";
			uni.removeStorageSync("token");
			uni.removeStorageSync("userInfo")
		},
	},
	actions: {
		async getToken({
			commit
		}, payload) {
			let userList = await toLogoin({
				principal: payload.code
			});
			commit("getUser", {
				token: userList.access_token,
				userInfo: payload.userInfo
			});
			// console.log(payload, "payload")
			// console.log(userList, 'userList');
		},
		async isLogin(context, payload) {
			// console.log("111111");
			if (context.state.token) return true; //已经登录
			let res = await uni.showModal({
				title: "登录",
				content: "登录后才能访问"
			});
			if(res[1].confirm){// 点击确定，跳转登录页面
				uni.navigateTo({
					url:"/searchpkg/login/login"
				})
			}

			return false; //没有登录
		}
	}
}

export default total;
