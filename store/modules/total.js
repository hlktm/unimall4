const count = {
	namespaced:true, //开启命名空间
	state:{
		count:0
	},
	mutations:{
		changeTotal(state,count){
			//更改total
			state.count = state.total + count
		},
		setCount(state,count){
			state.count = count
		}
	}
}

export default count;