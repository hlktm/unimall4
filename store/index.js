import Vue from 'vue'
import Vuex from 'vuex'
import shoper from './modules/total.js'
import user from './modules/user.js'
import order from './modules/order.js'
//1.安装插件
Vue.use(Vuex)
 
//2.创建对象
const store = new Vuex.Store({
  state:{},
  mytations:{},
  modules:{
    'car':shoper,//
	user,//用户登录信息
	order,//
    // shoper,
  }
})
//3.导出使用
export default store